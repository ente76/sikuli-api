package org.sikuli.api.audio;

import static java.applet.Applet.newAudioClip;
import java.applet.AudioClip;
import java.net.URL;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import static javax.media.Manager.createRealizedPlayer;
import javax.media.Player;

public class DesktopSpeaker implements Speaker{

	public static void main(String[] args) throws Exception {
		DesktopSpeaker sp = new DesktopSpeaker();
		URL url = new URL("file", null, "clickhere.wav");
		sp.play(url);
		sp.play(url);
	}

	public void play1(URL url){
		AudioClip clip = newAudioClip(url);
		clip.play();
	}

	@Override
	public void play(URL url)  {

		try {

			final Player player;
			final Object o = new Object();
			player = createRealizedPlayer(url);		
			player.addControllerListener((ControllerEvent e) -> {
                            if (e.getClass() == javax.media.EndOfMediaEvent.class){
                                synchronized(o){
                                    o.notify();
                                }
                                player.stop();
                                player.close();
                            }
                        });

			player.start();
			synchronized(o){
				o.wait();
			}
		}catch(Exception e){
			
		}
	}
}




