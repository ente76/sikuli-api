package org.sikuli.api.audio;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author doGhacker
 */
/* Program HelloJMF.java
Plays an audio file that is present in the same directory wherein the HelloJMF.class file resides.
*/
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.lang.System.exit;
import java.net.URL;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import static javax.media.Manager.createRealizedPlayer;
import javax.media.Player;
import javax.swing.JFrame;
import static org.sikuli.api.audio.HelloJMF.stop;

public class HelloJMF {

    JFrame frame = new JFrame(" Hello JMF Player");
    static Player helloJMFPlayer = null;

    public HelloJMF() {
        try { // method using URL
            URL url = new URL("file", null, "male.wav");
            helloJMFPlayer = createRealizedPlayer(url);
            helloJMFPlayer.addControllerListener(System.out::println);
        } catch (Exception e) {
            System.out.println(" Unable to create the audioPlayer :" + e);
        }
        Component control = helloJMFPlayer.getControlPanelComponent();
        frame.getContentPane().add(control, BorderLayout.CENTER);
        frame.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent we) {
                stop();
                exit(0);
            }
        });
        frame.pack();
        frame.setSize(new Dimension(200, 50));
        frame.setVisible(true);
        helloJMFPlayer.start();
    }

    public static void stop() {
        helloJMFPlayer.stop();
        helloJMFPlayer.close();
    }

    public static void main(String args[]) {
        HelloJMF helloJMF = new HelloJMF();
    }
}