package org.sikuli.api;

import static com.google.common.collect.Lists.newArrayList;
import com.google.common.primitives.Doubles;
import static com.google.common.primitives.Doubles.compare;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import static java.lang.Math.min;
import static java.util.Collections.sort;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MultiStateTarget extends DefaultTarget implements Target {

	Map<Target, Object> states = new HashMap<>();
	
	@Override
	protected List<ScreenRegion> getUnordredMatches(ScreenRegion screenRegion){
		final BufferedImage image = screenRegion.capture();
		
		// get matches for each state and add them to a combined list
		List<ScreenRegion> allMatches = newArrayList();
		for (Target t : states.keySet()){
			t.setLimit(getLimit());
			//List<ScreenRegion> matches = t.getUnordredMatches(screenRegion);
			List<ScreenRegion> matches = t.doFindAll(screenRegion);
			allMatches.addAll(matches);
		}
				
		// sort them by scores
		sort(allMatches, (ScreenRegion arg0, ScreenRegion arg1) -> compare(arg1.getScore(),arg0.getScore()));
		

		// add padding to account for mis-alignment
		for (ScreenRegion m : allMatches){
			Rectangle oldBounds = m.getBounds();
			Rectangle newBounds = new Rectangle();
			newBounds.width = oldBounds.width + 10;
			newBounds.height = oldBounds.height + 10;
			newBounds.x = oldBounds.x - 5;
			newBounds.y = oldBounds.y - 5;
			m.setBounds(newBounds);
		}
			
		
		int n = min(getLimit(), allMatches.size());
		List<ScreenRegion> results = allMatches.subList(0, n);
		
		for (ScreenRegion r : results){
			// copy state information
			r.getStates().putAll(states);		
		}
		
		return results;
	}

	public void addState(Target target, Object state){
		states.put(target,  state);
	}


}