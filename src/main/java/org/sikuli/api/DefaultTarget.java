package org.sikuli.api;

import static com.google.common.collect.Lists.newArrayList;
import java.awt.image.BufferedImage;
import static java.util.Collections.sort;
import java.util.Comparator;
import java.util.List;
import org.sikuli.core.search.RegionMatch;

/**
 * 
 * @author Tom Yeh (tom.yeh@colorado.edu)
 *
 */
abstract public class DefaultTarget implements Target {


	protected double getDefaultMinScore() { return 0;};
	protected int getDefaultLimit() { return 100;};
		
	/**
	 * Constructs a Target with default parameters
	 * 
	 */
	public DefaultTarget(){
		setMinScore(getDefaultMinScore());
		setLimit(getDefaultLimit());
	}

	public double getMinScore() {
		return minScore;
	}	

	/**
	 * Sets the minimum matching score. This controls how "fuzzy" the
	 * image matching should be. The score should be between 0 and 1 
	 * where 1 is the most precise (least fuzzy).
	 * 
	 * @param minScore
	 */
	public void setMinScore(double minScore) {
		this.minScore = minScore;
	}

	public int getLimit() {
		return limit;
	}
	
	/**
	 * Sets the limit on the number of matched targets to return.
	 * 
	 * @param limit	the number of matches
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	public Ordering getOrdering() {
		return ordering;
	}

	/**
	 * Sets the ordering of the matched targets.
	 * 
	 * @param ordering
	 */
	public void setOrdering(Ordering ordering) {
		this.ordering = ordering;
	}

	private double minScore = 0;
	private int limit = 0;
	private Ordering ordering = Ordering.DEFAULT;
	
	
	abstract protected List<ScreenRegion> getUnordredMatches(ScreenRegion screenRegion);
	
	protected static List<ScreenRegion> convertToScreenRegions(ScreenRegion parent, List<RegionMatch> rms) {
		List<ScreenRegion> irs = newArrayList();		
		for (RegionMatch rm : rms){
			ScreenRegion ir = new DefaultScreenRegion(parent, rm.getX(),rm.getY(),rm.getWidth(),rm.getHeight());
			ir.setScore(rm.getScore());
			irs.add(ir);
		}
		return irs;
	}
	
	@Override
	public List<ScreenRegion> doFindAll(ScreenRegion screenRegion) {
		// get raw results
		List<ScreenRegion> ScreenRegions = getUnordredMatches(screenRegion);

		// sorting
		if (ordering == Ordering.TOP_DOWN){
			sort(ScreenRegions, (ScreenRegion a, ScreenRegion b) -> a.getBounds().y - b.getBounds().y);
		}else if (ordering == Ordering.BOTTOM_UP){
			sort(ScreenRegions, (ScreenRegion a, ScreenRegion b) -> b.getBounds().y - a.getBounds().y);			
		}else if (ordering == Ordering.LEFT_RIGHT){
			sort(ScreenRegions, (ScreenRegion a, ScreenRegion b) -> a.getBounds().x - b.getBounds().x);			
		}else if (ordering == Ordering.RIGHT_LEFT){
			sort(ScreenRegions, (ScreenRegion a, ScreenRegion b) -> b.getBounds().x - a.getBounds().x);			
		}
		
		return ScreenRegions;
	}
	
	/**
	 * Gets the image representation of this target for visualization purposes
	 * 
	 * @return a BufferedImage object
	 */
	public BufferedImage toImage() {
		return null;
	}
		
}