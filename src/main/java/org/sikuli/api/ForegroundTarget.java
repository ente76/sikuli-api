package org.sikuli.api;

import static com.google.common.collect.Lists.newArrayList;
import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.IplImage.createFrom;
import java.awt.image.BufferedImage;
import static java.lang.String.format;
import java.util.List;
import static org.sikuli.core.cv.VisionUtils.computeForegroundMaskOf;
import static org.sikuli.core.cv.VisionUtils.detectBlobs;

public class ForegroundTarget extends DefaultTarget implements Target {

	public ForegroundTarget() {
	}

	@Override
	protected List<ScreenRegion> getUnordredMatches(ScreenRegion screenRegion) {
		
		BufferedImage image = screenRegion.capture();
		IplImage foregroundMask = computeForegroundMaskOf(createFrom(image));
		List<CvRect> blobs = detectBlobs(foregroundMask);
		
		List<ScreenRegion> results = newArrayList();
		for (CvRect b : blobs){
			System.out.println(format("%d,%d,%d,%d",b.x(),b.y(),b.width(),b.height()));
			ScreenRegion r = screenRegion.getRelativeScreenRegion(b.x(), b.y(), b.width(), b.height());
			results.add(r);
		}		
		return results;
	}

}