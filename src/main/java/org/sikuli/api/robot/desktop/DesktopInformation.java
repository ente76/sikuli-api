/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sikuli.api.robot.desktop;

import static org.sikuli.api.robot.desktop.AWTDesktop.getScreenBounds;

/**
 *
 * @author ente
 */
public class DesktopInformation {

    /*
     * retrieves the number of available screens
     */
    public static Integer getNumberScreens() {
        return AWTDesktop.getNumberScreens();
    }

    /*
     * determines the ScreenID related to some coordinates X and Y
     * returns null if coordinates reside outside of all available screens
     */
    public static DesktopScreen getScreenAtCoord(Integer x, Integer y) {
        DesktopScreen _screen=null;
        for (Integer i=0; i<getNumberScreens(); i++) {
            if (getScreenBounds(i).contains(x, y)) {
                _screen=new DesktopScreen(i);
            }
        }
        return _screen;
    }

}
