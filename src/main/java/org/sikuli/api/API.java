package org.sikuli.api;

import static java.awt.Desktop.getDesktop;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class API {
	
	static public void pause(int mills){		
		try {
			sleep(mills);
		} catch (InterruptedException e) {
		}		
	}

	static public void browse(URL url) {
		try {
			getDesktop().browse(new URI(url.toString()));
		} catch (URISyntaxException | IOException e) {
		}
	}
	
}
