package org.sikuli.api;

import java.awt.image.BufferedImage;
import java.io.File;
import static java.lang.Math.min;
import java.net.URL;
import java.util.List;
import static org.sikuli.api.FourCornerModel.learnFrom;
import static org.sikuli.api.VisualModelFinder.searchButton;
import org.sikuli.core.search.RegionMatch;

public class StyledRectangleTarget extends ImageTarget {
	
	public StyledRectangleTarget(URL url) {
		super(url);
	}

	public StyledRectangleTarget(File file) {
		super(file);
	}
	
	public StyledRectangleTarget(BufferedImage image) {
		super(image);
	}


	@Override
	protected List<ScreenRegion> getUnordredMatches(ScreenRegion screenRegion){
		BufferedImage exampleImage = getImage();
		FourCornerModel buttonModel = learnFrom(exampleImage);
		List<RegionMatch> matches = searchButton(buttonModel, 
				screenRegion.capture());	
		List<RegionMatch> subList = matches.subList(0,  min(getLimit(), matches.size()));
		return convertToScreenRegions(screenRegion, subList);
	}

}
