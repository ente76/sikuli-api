package org.sikuli.api.visual;

import static com.sun.awt.AWTUtilities.setWindowOpaque;
import edu.umd.cs.piccolo.PCanvas;
import javax.swing.JWindow;

public class ScreenOverlayWindow extends JWindow implements ScreenDisplayable {

	private final PCanvas canvas;
	public ScreenOverlayWindow() {
		canvas = new PCanvas();
		canvas.setBackground(null);
                canvas.setOpaque(false); //this line is needed to make the window transparent on Windows 7 64bit Java 1.7
		add(canvas);
//		setBackground(null);
		getContentPane().setBackground(null); // this line is needed to make the window transparent on Windows
		setWindowOpaque(this, false);
		setAlwaysOnTop(true);
	}

	public PCanvas getCanvas(){
		return canvas;
	}

	@Override
	public void displayOnScreen() {
		setVisible(true);

	}

	@Override
	public void hideFromScreen() {
		setVisible(false);
		dispose();
	}

}